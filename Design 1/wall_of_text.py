import random


set_of_word = [
    "Energiedemokratie",
    "Ernährungssouveränität",
    "freier Seenzugang",
    "Klimaschutz",
    "Verteilungsgerechtigkeit",
    "Verkehrswende",
]

last_word = ""
count = 0

for i in range(0, 100):
    word = set_of_word[random.randint(0, len(set_of_word) - 1)]
    if word is not last_word:
        print(f"{word} //", end=" ")
        count += 1
        if count % 5 == 0:
            print()
        
    last_word = word
    
